<?php
use PEAR2\Net\RouterOS;
require_once dirname(__FILE__).'/PEAR2_Net_RouterOS-1.0.0b5.phar';

include_once dirname(__FILE__).'/../connection.php';
$queryMk = "SELECT usuario, pass, nombre FROM usuarios WHERE idUsuarios = 49";
$resultMk = mysqli_query($con, $queryMk);
$rowMk = mysqli_fetch_assoc($resultMk);

$mac = $row['num_equipo'];
//$mac = $_GET['mac'];
$mac = str_replace(":","",$mac);
$mac = wordwrap($mac , 2 , ':' , true );
$mac = strtoupper($mac);

try {
    $client = new RouterOS\Client('148.240.94.2', $rowMk['usuario'], $rowMk['pass'], $rowMk['nombre']);
    //$client = new RouterOS\Client('148.240.94.2', "mikerdz", "LAfs=%PM6uNz!eS-");

    /*************************************************************
    ******************Obtenemos datos del usuario*****************
    **************************************************************/
    $printRequest = new RouterOS\Request('/tool/user-manager/user/print');
    $printRequest->setArgument('.proplist', '.id,actual-profile,username');
    $printRequest->setQuery(RouterOS\Query::where('username', $mac));
    $user_id = $client->sendSync($printRequest)->getProperty('.id');
    $user_profile = $client->sendSync($printRequest)->getProperty('actual-profile');
    $user_name = $client->sendSync($printRequest)->getProperty('username');
    $user_speed = "";

    $num_equipo = str_replace(":","",$user_name);

    if ($user_profile == '1Mega' || $user_profile == '1Vencido'){
        $user_speed = "1 MB";
    } else if ($user_profile == '2Megas' || $user_profile == '2Vencido'){
        $user_speed = "2 MB";
    } else if ($user_profile == '3Megas' || $user_profile == '3Vencido'){
        $user_speed = "3 MB";
    } else if ($user_profile == '4Megas' || $user_profile == '4Vencido'){
        $user_speed = "4 MB";
    } else if ($user_profile == '5Megas' || $user_profile == '5Vencido'){
        $user_speed = "5 MB";
    } else if ($user_profile == '6Megas' || $user_profile == '6Vencido'){
        $user_speed = "6 MB";
    } else if ($user_profile == '8Megas' || $user_profile == '8Vencido'){
        $user_speed = "8 MB";
    } else if ($user_profile == '10Megas' || $user_profile == '10vencido'){
        $user_speed = "10 MB";
    } else if ($user_profile == '15Megas' || $user_profile == '15Vencido'){
        $user_speed = "15 MB";
    }

    if ($user_speed == "") {
        $user_speed = $row["velocidad"];
    }

} catch (RouterOS\SocketException $e) {
    echo 'Falló la conección con RouterOS... ' . $e;
} catch (RouterOS\DataFlowException $e) {
    echo $e->getMessage();//Wrong username or password; probably
} catch (Exception $e) {
    echo '<strong>Error desconocido, comuníquese con el administrador (Mike)...</strong><br><br> ' . $e; //Connection fail to MySQL; probably
} 