<?php

use PEAR2\Net\RouterOS;
require_once 'PEAR2_Net_RouterOS-1.0.0b5.phar';

include_once dirname(__FILE__).'/../connection.php';
$queryMk = "SELECT usuario, pass, nombre FROM usuarios WHERE idUsuarios = 49";
$resultMk = mysqli_query($con, $queryMk);
$rowMk = mysqli_fetch_assoc($resultMk);

$mac = $_POST['num_equipo'];
$mac = str_replace(":","",$mac);
$mac = wordwrap($mac , 2 , ':' , true );
$mac = strtoupper($mac);

/*if ($cambioPerfil == false) {
    $perfil = $_POST['paquete'];
}*/

if (isset($_POST['nuevo'])) {       /////////////NO ES PROSPECTO
    $perfil = $_POST['paquete'];
}

try {
    $client = new RouterOS\Client('148.240.94.2', $rowMk['usuario'], $rowMk['pass'], $rowMk['nombre']);
    $client2 = new RouterOS\Client('148.240.94.3', $rowMk['usuario'], $rowMk['pass'], $rowMk['nombre']);
    $client3 = new RouterOS\Client('148.240.94.4', $rowMk['usuario'], $rowMk['pass'], $rowMk['nombre']);
    $client4 = new RouterOS\Client('148.240.94.5', $rowMk['usuario'], $rowMk['pass'], $rowMk['nombre']);
    $client5 = new RouterOS\Client('148.240.94.6', $rowMk['usuario'], $rowMk['pass'], $rowMk['nombre']);

    /*******************************************************
    ********************Agregando cliente*******************
    ********************************************************/
    echo "creando nuevo cliente";
    $activateRequest = new RouterOS\Request('/tool user-manager user add');
    $activateRequest
        ->setArgument('username', $mac)
        ->setArgument('password', 'D1LcO16')
        ->setArgument('customer', 'admin');
    $client->sendSync($activateRequest);
    echo "<strong>LISTO</strong><br><br>";
    

    /*******************************************************
    ******************Obtenemos ID del user*****************
    ********************************************************/
    $printRequest = new RouterOS\Request('/tool/user-manager/user/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('username', $mac));
    $id = $client->sendSync($printRequest)->getProperty('.id');
    echo "<strong>LISTO</strong><br><br>";
    //$id now contains the ID of the entry we're targeting

    /*******************************************************
    *****Cambiamos el perfil del cliente en user-manager****
    ********************************************************/
    echo "Cambiando el perfil del cliente...";
    $activateRequest = new RouterOS\Request('/tool/user-manager/user/create-and-activate-profile');
    $activateRequest
        ->setArgument('customer', 'admin')
        ->setArgument('profile', $perfil)
        ->setArgument('numbers', $id);
    $client->sendSync($activateRequest);
    echo "<strong>LISTO</strong><br><br>";

    /*******************************************************
    ******************Obtenemos ID del host*****************
    ********************************************************/
    $printRequest = new RouterOS\Request('/ip/hotspot/host/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('mac-address', $mac));
    $id3 = $client->sendSync($printRequest)->getProperty('.id');

    $printRequest = new RouterOS\Request('/ip/hotspot/host/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('mac-address', $mac));
    $id4 = $client2->sendSync($printRequest)->getProperty('.id');

    $printRequest = new RouterOS\Request('/ip/hotspot/host/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('mac-address', $mac));
    $id5 = $client3->sendSync($printRequest)->getProperty('.id');

    $printRequest = new RouterOS\Request('/ip/hotspot/host/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('mac-address', $mac));
    $id6 = $client4->sendSync($printRequest)->getProperty('.id');

    $printRequest = new RouterOS\Request('/ip/hotspot/host/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('mac-address', $mac));
    $id7 = $client5->sendSync($printRequest)->getProperty('.id');

    //$id2 now contains the ID of the entry we're targeting

    /**************************************************************
    Desconectamos al usuario para que se active con el nuevo perfil (los dos clientes)
    ***************************************************************/
    echo "Desconectando al cliente y volviéndolo a conectar para reflejar los cambios...";
    $removeRequest = new RouterOS\Request('/ip/hotspot/host/remove');
    $removeRequest->setArgument('numbers', $id3);
    $responses = $client->sendSync($removeRequest);
    
    $removeRequest2 = new RouterOS\Request('/ip/hotspot/host/remove');
    $removeRequest2->setArgument('numbers', $id4);
    $responses2 = $client2->sendSync($removeRequest2);
    
    $removeRequest3 = new RouterOS\Request('/ip/hotspot/host/remove');
    $removeRequest3->setArgument('numbers', $id5);
    $responses3 = $client3->sendSync($removeRequest3);
    
    $removeRequest4 = new RouterOS\Request('/ip/hotspot/host/remove');
    $removeRequest4->setArgument('numbers', $id6);
    $responses4 = $client4->sendSync($removeRequest4);
    
    $removeRequest5 = new RouterOS\Request('/ip/hotspot/host/remove');
    $removeRequest5->setArgument('numbers', $id7);
    $responses5 = $client5->sendSync($removeRequest5);

    echo "<strong>LISTO. Perfil Actualizado.</strong><br><br>";

} catch (RouterOS\SocketException $e) {
    echo 'Falló la conección con RouterOS... ' . $e;
} catch (RouterOS\DataFlowException $e) {
    echo $e->getMessage();//Wrong username or password; probably
} catch (Exception $e) {
    echo 'Error desconocido, comuníquese con el administrador (Mike)... ' . $e; //Connection fail to MySQL; probably
} 