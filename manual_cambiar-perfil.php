<?php
use PEAR2\Net\RouterOS;
require_once dirname(__FILE__).'/PEAR2_Net_RouterOS-1.0.0b5.phar';

include_once dirname(__FILE__).'/../connection.php';
$queryMk = "SELECT usuario, pass, nombre FROM usuarios WHERE idUsuarios = 49";
$resultMk = mysqli_query($con, $queryMk);
$rowMk = mysqli_fetch_assoc($resultMk);

$mac = $row['num_equipo'];
$mac = str_replace(":","",$mac);
$mac = wordwrap($mac , 2 , ':' , true );
$mac = strtoupper($mac);

$perfil = $_POST['paquete'];
$cambioPerfil = true;

try {
    $client = new RouterOS\Client('148.240.94.2', $rowMk['usuario'], $rowMk['pass'], $rowMk['nombre']);
    $client2 = new RouterOS\Client('148.240.94.3', $rowMk['usuario'], $rowMk['pass'], $rowMk['nombre']);
    $client3 = new RouterOS\Client('148.240.94.4', $rowMk['usuario'], $rowMk['pass'], $rowMk['nombre']);
    $client4 = new RouterOS\Client('148.240.94.5', $rowMk['usuario'], $rowMk['pass'], $rowMk['nombre']);
    $client5 = new RouterOS\Client('148.240.94.6', $rowMk['usuario'], $rowMk['pass'], $rowMk['nombre']);

    /*******************************************************
    ******************Obtenemos ID del user*****************
    ********************************************************/
    $printRequest = new RouterOS\Request('/tool/user-manager/user/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('username', $mac));
    $id = $client->sendSync($printRequest)->getProperty('.id');

    $printRequest = new RouterOS\Request('/tool/user-manager/user/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('username', $mac));
    $id3 = $client2->sendSync($printRequest)->getProperty('.id');

    $printRequest = new RouterOS\Request('/tool/user-manager/user/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('username', $mac));
    $id5 = $client3->sendSync($printRequest)->getProperty('.id');

    $printRequest = new RouterOS\Request('/tool/user-manager/user/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('username', $mac));
    $id7 = $client4->sendSync($printRequest)->getProperty('.id');

    $printRequest = new RouterOS\Request('/tool/user-manager/user/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('username', $mac));
    $id9 = $client5->sendSync($printRequest)->getProperty('.id');
    //$id now contains the ID of the entry we're targeting

    /*******************************************************
    *****Cambiamos el perfil del cliente en user-manager****
    ********************************************************/
    echo "Cambiando el perfil del cliente...";
    $activateRequest = new RouterOS\Request('/tool/user-manager/user/create-and-activate-profile');
    $activateRequest
        ->setArgument('customer', 'admin')
        ->setArgument('profile', $perfil)
        ->setArgument('numbers', $id);
    $client->sendSync($activateRequest);

    $activateRequest = new RouterOS\Request('/tool/user-manager/user/create-and-activate-profile');
    $activateRequest
        ->setArgument('customer', 'admin')
        ->setArgument('profile', $perfil)
        ->setArgument('numbers', $id3);
    $client2->sendSync($activateRequest);

    $activateRequest = new RouterOS\Request('/tool/user-manager/user/create-and-activate-profile');
    $activateRequest
        ->setArgument('customer', 'admin')
        ->setArgument('profile', $perfil)
        ->setArgument('numbers', $id5);
    $client3->sendSync($activateRequest);

    $activateRequest = new RouterOS\Request('/tool/user-manager/user/create-and-activate-profile');
    $activateRequest
        ->setArgument('customer', 'admin')
        ->setArgument('profile', $perfil)
        ->setArgument('numbers', $id7);
    $client4->sendSync($activateRequest);

    $activateRequest = new RouterOS\Request('/tool/user-manager/user/create-and-activate-profile');
    $activateRequest
        ->setArgument('customer', 'admin')
        ->setArgument('profile', $perfil)
        ->setArgument('numbers', $id9);
    $client5->sendSync($activateRequest);
    echo "<strong>LISTO</strong><br><br>";

    /*******************************************************
    ******************Obtenemos ID del host*****************
    ********************************************************/
    $printRequest = new RouterOS\Request('/ip/hotspot/host/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('mac-address', $mac));
    $id2 = $client->sendSync($printRequest)->getProperty('.id');

    $printRequest = new RouterOS\Request('/ip/hotspot/host/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('mac-address', $mac));
    $id4 = $client2->sendSync($printRequest)->getProperty('.id');

    $printRequest = new RouterOS\Request('/ip/hotspot/host/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('mac-address', $mac));
    $id6 = $client3->sendSync($printRequest)->getProperty('.id');

    $printRequest = new RouterOS\Request('/ip/hotspot/host/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('mac-address', $mac));
    $id8 = $client4->sendSync($printRequest)->getProperty('.id');

    $printRequest = new RouterOS\Request('/ip/hotspot/host/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('mac-address', $mac));
    $id10 = $client5->sendSync($printRequest)->getProperty('.id');
    //$id2 now contains the ID of the entry we're targeting

    /**************************************************************
    Desconectamos al usuario para que se active con el nuevo perfil
    ***************************************************************/
    echo "Desconectando al cliente y volviéndolo a conectar para reflejar los cambios...";
    $removeRequest = new RouterOS\Request('/ip/hotspot/host/remove');
    $removeRequest->setArgument('numbers', $id2);
    $client->sendSync($removeRequest);

    $removeRequest2 = new RouterOS\Request('/ip/hotspot/host/remove');
    $removeRequest2->setArgument('numbers', $id4);
    $client2->sendSync($removeRequest2);

    $removeRequest3 = new RouterOS\Request('/ip/hotspot/host/remove');
    $removeRequest3->setArgument('numbers', $id6);
    $client3->sendSync($removeRequest3);

    $removeRequest4 = new RouterOS\Request('/ip/hotspot/host/remove');
    $removeRequest4->setArgument('numbers', $id8);
    $client4->sendSync($removeRequest4);

    $removeRequest5 = new RouterOS\Request('/ip/hotspot/host/remove');
    $removeRequest5->setArgument('numbers', $id10);
    $client5->sendSync($removeRequest5);

    echo "<strong>LISTO (manual_cambiar-perfil).. Perfil Actualizado.</strong><br><br>";


} catch (RouterOS\SocketException $e) {
    echo 'Falló la conección con RouterOS... ' . $e;
} catch (RouterOS\DataFlowException $e) {
    echo $e->getMessage();//Wrong username or password; probably
} catch (Exception $e) {
    echo '<strong>Error desconocido, comuníquese con el administrador (Mike)...</strong><br><br> ' . $e; //Connection fail to MySQL; probably
} 