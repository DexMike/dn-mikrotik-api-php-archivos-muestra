<?php
use PEAR2\Net\RouterOS;
require_once dirname(__FILE__).'/PEAR2_Net_RouterOS-1.0.0b5.phar';

include_once dirname(__FILE__).'/../connection.php';
$queryMk = "SELECT usuario, pass, nombre FROM usuarios WHERE idUsuarios = 49";
$resultMk = mysqli_query($con, $queryMk);
$rowMk = mysqli_fetch_assoc($resultMk);

$mac = $row['num_equipo'];
//$mac = $_GET['mac'];
$mac = str_replace(":","",$mac);
$mac = wordwrap($mac , 2 , ':' , true );
$mac = strtoupper($mac);

try {
    $client = new RouterOS\Client('148.240.94.2', $rowMk['usuario'], $rowMk['pass'], $rowMk['nombre']);

    /*******************************************************
    ******************Obtenemos ID del user*****************
    ********************************************************/
    $printRequest = new RouterOS\Request('/tool/user-manager/user/print');
    $printRequest->setArgument('.proplist', '.id');
    $printRequest->setQuery(RouterOS\Query::where('username', $mac));
    $id = $client->sendSync($printRequest)->getProperty('.id');
    //$id now contains the ID of the entry we're targeting

    /*******************************************************
    ******************Eliminamos al usuario*****************
    ********************************************************/
    echo "Eliminando cliente...";
    $removeRequest = new RouterOS\Request('/tool/user-manager/user/remove');
    $removeRequest
        ->setArgument('numbers', $id);
    $client->sendSync($removeRequest);

    /**************************************************************
    Desconectamos al usuario para que se active con el nuevo perfil
    ***************************************************************/
    /*echo "Desconectando al cliente y volviéndolo a conectar para reflejar los cambios...";
    $removeRequest = new RouterOS\Request('/ip/hotspot/host/remove');
    $removeRequest->setArgument('numbers', $id2);
    $client->sendSync($removeRequest);*/
    echo "<strong>LISTO. Cliente eliminado.</strong><br><br>";


} catch (RouterOS\SocketException $e) {
    echo 'Falló la conección con RouterOS... ' . $e;
} catch (RouterOS\DataFlowException $e) {
    echo $e->getMessage();//Wrong username or password; probably
} catch (Exception $e) {
    echo '<strong>Error desconocido, comuníquese con el administrador (Mike)...</strong><br><br> ' . $e; //Connection fail to MySQL; probably
} 